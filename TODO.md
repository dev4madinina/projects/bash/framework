# TODO list


## To do

- [ ] README.md
  - [ ] Docker
    - [ ] Ajouter un volume pour récupérer le `/home` du conteneur ~0.1d #doc @devmadinina 2024-02-08
    - [ ] Changer l'utilisateur du conteneur _(actuellement `root`)_ ~0.1d #doc @devmadinina 2024-02-08
- [ ] Procédure de désinstallation ~1d #feat @devmadinina 2024-02-08
- [ ] Script de lancement des tests unitaires ~1d #feat @devmadinina 2024-02-08
- [ ] Générer plusieurs _saveurs_ du framework _(femto, pico, nano...)_ ~2d #feat @devmadinina 2024-02-08

## In Progress

- [ ] Procédure d'installation ~1d #feat @devmadinina 2024-02-06

## Done ✓

- [x] Créer le TODO.md ~0.1d #doc @devmadinina 2024-02-08


## Sources

- [GitHub : todomd/todo.md](https://github.com/todomd/todo.md) &rarr; [exemple](https://github.com/todomd/todo.md/blob/master/TODO.md?plain=1)
